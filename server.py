#!/usr/bin/env python
from flask import Flask, render_template
from flask_restful import Resource, Api

'''Uma API RESTful para converter números na sua forma extensa (pt-BR).

Essa implementação de API RESTful possui simplesmente um endpoint '/'
que responde a requisições de tipo GET. O usuário faz a requisição de um
número compreendido entre [-MAX_NUM, MAX_NUM] (MAX_NUM padrão: 99999) e o
servidor retorna o número por extenso no formato JSON, sob a chave 'extenso'.

Entrada
-------
/numero :               Um número inteiro via GET.

Retorna
-------
{'extenso' : string} :  JSON com o número escrito por extenso
                        na chave 'extenso'.

Dependências
------------
flask e flask-restful
'''

__VERSION__ = 'v0.1a'
PORT = 3000
MAX_NUM = 99999

app = Flask(__name__)
api = Api(app)

# Dicionário com todos os radicais dos números por extenso
numerosPorExtenso = {
    'centenas' : ['', 'cento',
                       'duzentos',
                       'trezentos',
                       'quatrocentos',
                       'quinhentos',
                       'seiscentos',
                       'setecentos',
                       'oitocentos',
                       'novecentos'],
    # Espaços em vazio pois o algorítmo usa o índice da lista
    'dezenas' : ['',
                '',
                'vinte',
                'trinta',
                'quarenta',
                'cinquenta',
                'sessenta',
                'setenta',
                'oitenta',
                'noventa'],
    'digitos' : ['zero',
                  'um',
                  'dois',
                  'três',
                  'quatro',
                  'cinco',
                  'seis',
                  'sete',
                  'oito',
                  'nove',
                  'dez',
                  'onze',
                  'doze',
                  'treze',
                  'quatorze',
                  'quinze',
                  'dezesseis',
                  'dezessete',
                  'dezoito',
                  'dezenove'],
        }

class ForaDoIntervalo(Exception):
    '''Exceção definida caso o número esteja fora do intervalo
    [-MAX_NUM, MAX_NUM].

    Atributos
    ---------
    name :      Nome da exceção (estático: "ForaDoIntervalo")
    reason :    Razão da exceção (estática: "Número deve estar entre
                                             [-MAX_NUM, MAX_NUM]")
    '''

    def __init__(self):
        self.name  = 'ForaDoIntervalo'
        self.reason = 'Número deve estar entre [-{}, {}]'.format(MAX_NUM,MAX_NUM)

def checkRange(num:int):
    '''Verifica se o número está compreendido entre [-MAX_NUM, MAX_NUM], caso
    contrário, levanta exceção 'ForaDoIntervalo'.'''
    if num < -MAX_NUM or num > MAX_NUM:
        raise ForaDoIntervalo


def sinalEmExtenso(num:int) -> str:
    '''Checa se o número é negativo e retorna 'menos' por extenso caso positivo
    e nada caso negativo.

    Exemplos
    --------
    >>>sinalEmExtenso(-412)
    'menos'
    >>>sinalEmExtenso(123)
    ''
    '''
    if num < 0:
        return 'menos'
    else:
        return ''
def milharesEmExtenso(dezena:int) -> str:
    '''Dado uma dezena de milhar, retorna o número por extenso mais 'mil'.

    Por exemplo, a dezena de milhar de 41234 é 41.

    Retorna
    -------
    string: Dezena de milhar por extenso.

    Exemplo
    -------
    >>>milharesEmExtenso(32)
    'trinta e dois mil'
    '''
    return dezenasEmExtenso(dezena) + ' mil'

def centenasEmExtenso(unidade:int) -> str:
    '''Dado um dígito de centena, retorna o número por extenso de acordo com o
    dicionário 'numerosPorExtenso' definido globalmente.

    Retorna
    -------
    string: Centena por extenso.

    Exemplo
    -------
    >>>centenasEmExtenso(4)
    'quatrocentos'
    '''
    return numerosPorExtenso['centenas'][unidade%10]
def dezenasEmExtenso(num:int) -> str:
    '''Dado uma dezena, retorna o número por extenso de acordo com o dicionário
    'numerosPorExtenso' definido globalmente.

    Retorna
    -------
    string: Dezena por extenso>

    Exemplo
    -------
    >>>dezenasEmExtenso(67)
    'sessenta e sete'
    '''
    string = ''
    ten  = int(num/10)
    unit = num%10
    if num < 20:
        string += numerosPorExtenso['digitos'][num]
    else:
        string += numerosPorExtenso['dezenas'][ten]
        if unit != 0:
            string += ' e ' + numerosPorExtenso['digitos'][unit]
    return string


def extrairPartes(num:int) -> int:
    '''Dado um número 'num' entre [-MAX_NUM,MAX_NUM], extrai as partes de milhares,
    centenas e dezenas.

    Retorna
    -------
    tupla: (milhares, centenas, dezenas).

    Exemplo
    -------
    >>>extrairPartes(41232)
    (41,2,32)
    '''
    mil = int(num/1000)
    cem = int(num%1000/100)
    dez = int(num%100)
    return (mil,cem,dez)


def castToExtense(num):
    '''Dado um número entre [-MAX_NUM, MAX_NUM], retorna ele escrito por extenso
    em português.

    Retorna
    -------
    string: Número por extenso.

    Exemplos
    --------
    >>>castToExtense(-2133)
    'menos dois mil cento e trinta e três'

    >>>castToExtense(514)
    'quinhentos e quatorze'
    '''

    # Pequeno 'hack' para tratar rapidamente os únicos casos estranhos
    if num == 0:
        return 'zero'
    elif num == 100:
        return 'cem'
    milhares,centenas,dezenas = extrairPartes(abs(num))

    string = sinalEmExtenso(num)
    if milhares != 0:
        if string != '': string += ' '
        string += milharesEmExtenso(milhares)
    if centenas != 0:
        if string != '': string += ' '
        string += centenasEmExtenso(centenas)
    if dezenas != 0:
        if string != '': string += ' e '
        string += dezenasEmExtenso(dezenas)

    return string


class Extenso(Resource):
    '''
    Implementação da classe abstrata 'Resource' do Flask-RESTful.

    Nesse recurso apenas o método GET for especializado para implementar o
    endpoint de conversão de números cardinais para a forma extensa, em
    português.
    '''
    def get(self, num):
        '''Especialização do método GET para conversão de números.

        Caso o número não esteja compreendido no intervalo [-MAX_NUM,MAX_NUM],
        retorna '{'extenso': 'ForaDoIntervalo'}'

        Entrada
        -------
        num : int
            Número inteiro passado via GET para o servidor.

        Saída
        -----
        JSON : {'extenso': str}
            Retorna o número escrito por extenso.
        '''
        try:
            checkRange(num)
            return {'extenso': castToExtense(num)}
        except ForaDoIntervalo as e:
            return {'extenso': e.name}

# Configura root '/' para mostrar página de instruções
@app.route('/')
def index():
    return render_template('usage.html', ver=__VERSION__,
                                         maxNum=MAX_NUM)

# Configura o endpoint no servidor
api.add_resource(Extenso, '/<int(signed=True):num>', endpoint='extenso')

if __name__ == "__main__":
    # Roda com werkzeug do Flask
    app.run(port=PORT)
