# Conversão de Números para escrita extensa

Repositório referente ao desafio técnico de desenvolvedor da _**Fundação CERTI**_.

Implementação de um servidor API [**RESTful**](https://en.wikipedia.org/wiki/Representational_state_transfer) para conversão de números cardinais em sua escrita por extenso em português.

Quando o usuário entra com uma requisição **GET** informando um número compreendido entre `[-99999,99999]`, o servidor retorna um **JSON** contendo o número descrito por extenso na língua portuguesa na chave `extenso`.

### Exemplo:
>
```
http://127.0.0.1:3000/3216
{'extenso': 'três mil duzentos e dezesseis'}
```
>

Por padrão o servidor escuta a porta 3000. Quando o endpoint é a própria raíz, o servidor mostra uma página com instruções.


## Linguagem e bibliotecas
O programa todo é feito em **Python** e usa apenas duas bibliotecas para a configuração da API RESTful

* Python
    + [Flask](http://flask.pocoo.org/)
    + [Flask-restful](https://flask-restful.readthedocs.io/en/latest/)


## Configuração
De modo a desenvolver o software apenas com as bibliotecas essenciais, é boa prática criar um ambiente virtual e instalar apenas as bibliotecas que serão utilizadas, evitando assim a contaminação de bibliotecas locais numa etapa posterior de produção. Incluo no repositório o `virtualenv`, então você pode ativá-lo execudando o comando

>`$ source ExtensoVENV/bin/activate`

na raíz do repositório. Caso deseje criar o `virtualenv`, instale também os pacotes dentro do arquivo `requirements.txt` executando

>`$ pip install -r requirements.txt`

Assim que todas as dependências já estiverem resolvidas, basta executar

>`$ ./server.py`

na raíz do repositório para iniciar o servidor, que responderá, por padrão, em http://127.0.0.1:3000. Esse comando roda uma instância do `werkzeug` para começar a servir HTML.


Incluo também uma imagem [Docker](https://docker.com/), denominada **desafio-certi**, para facilmente habilitar o servidor. Para carregá-la ao `docker`, execute

>`$ docker load -i desafio-certi-docker.tar`

e depois crie um container mapeando a porta 3000 em alguma outra, por exemplo

>`$ docker run -p 4000:3000 desafio-certi`


## Uso
Para utilizar o servidor, basta enviar requisições GET ao servidor pela porta mapeada no endpoint http://127.0.0.1:3000/numero. Por exemplo,

>`http://127.0.0.1:3000/-3123`

Entre em http://127.0.0.1:3000/ para obter informações básicas de uso.